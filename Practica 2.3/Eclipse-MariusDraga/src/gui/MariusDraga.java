package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;
import javax.swing.JTree;
import javax.swing.JSeparator;
import javax.swing.JPasswordField;
import java.awt.Toolkit;
import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.Icon;
/*
 * @author Marius Draga
 * 
 *  @since 31-01-2018
 * 
 * 
 */
public class MariusDraga extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private JTextField textField_1;
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private final ButtonGroup buttonGroup_4 = new ButtonGroup();
	private final ButtonGroup buttonGroup_5 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MariusDraga frame = new MariusDraga();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MariusDraga() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MariusDraga.class.getResource("/Images/asses-snowboards-selective-coloring-women.jpg")));
		setTitle("Riders");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 612, 493);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmEditar = new JMenuItem("Guardar");
		mnArchivo.add(mntmEditar);
		
		JMenuItem mntmInformacion = new JMenuItem("Modificar");
		mnArchivo.add(mntmInformacion);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmCambiarVentana = new JMenuItem("Cambiar Ventana");
		mnEditar.add(mntmCambiarVentana);
		
		JMenuItem menuItem = new JMenuItem("");
		mnEditar.add(menuItem);
		
		JMenu mnNavegar = new JMenu("Navegar");
		menuBar.add(mnNavegar);
		
		JMenu mnEjecutar = new JMenu("Ejecutar");
		menuBar.add(mnEjecutar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(5, 5, 586, 36);
		contentPane.add(toolBar);
		
		JButton btnAbrir = new JButton("Abrir");
		toolBar.add(btnAbrir);
		
		JButton btnGuardar = new JButton("Guardar");
		toolBar.add(btnGuardar);
		
		JButton btnCerrar = new JButton("Cerrar");
		toolBar.add(btnCerrar);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(15, 45, 576, 377);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Riders", (Icon) null, panel, null);
		panel.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(10, 11, 129, 52);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNombreAlumno_1 = new JButton("Nombre Rider");
		btnNombreAlumno_1.setForeground(Color.BLUE);
		btnNombreAlumno_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNombreAlumno_1.setBounds(162, 40, 144, 23);
		panel.add(btnNombreAlumno_1);
		
		JCheckBox chckbxBeca_1 = new JCheckBox("Apto");
		buttonGroup_4.add(chckbxBeca_1);
		chckbxBeca_1.setBounds(10, 106, 97, 23);
		panel.add(chckbxBeca_1);
		
		JRadioButton rdbtnHombre_1 = new JRadioButton("Hombre");
		buttonGroup_3.add(rdbtnHombre_1);
		rdbtnHombre_1.setBounds(10, 157, 109, 23);
		panel.add(rdbtnHombre_1);
		
		JRadioButton rdbtnMujer_1 = new JRadioButton("Mujer");
		buttonGroup_3.add(rdbtnMujer_1);
		rdbtnMujer_1.setBounds(124, 157, 109, 23);
		panel.add(rdbtnMujer_1);
		
		JCheckBox chckbxNoApto = new JCheckBox("No Apto");
		buttonGroup_4.add(chckbxNoApto);
		chckbxNoApto.setBounds(112, 106, 97, 23);
		panel.add(chckbxNoApto);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Sponsors", null, panel_1, null);
		panel_1.setLayout(null);
		
		JButton btnNombreAlumno = new JButton("Nombre Esponsor");
		btnNombreAlumno.setForeground(Color.BLUE);
		btnNombreAlumno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNombreAlumno.setBounds(162, 40, 144, 23);
		panel_1.add(btnNombreAlumno);
		
		textField = new JTextField();
		textField.setBounds(10, 11, 129, 52);
		panel_1.add(textField);
		textField.setColumns(10);
		
		JCheckBox chckbxBeca = new JCheckBox("Sponsor Nacional");
		buttonGroup_5.add(chckbxBeca);
		chckbxBeca.setBounds(10, 106, 129, 23);
		panel_1.add(chckbxBeca);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		buttonGroup_2.add(rdbtnHombre);
		rdbtnHombre.setBounds(10, 157, 109, 23);
		panel_1.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		buttonGroup_2.add(rdbtnMujer);
		rdbtnMujer.setBounds(124, 157, 109, 23);
		panel_1.add(rdbtnMujer);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Sponsor Internacional");
		buttonGroup_5.add(chckbxNewCheckBox);
		chckbxNewCheckBox.setBounds(146, 106, 146, 23);
		panel_1.add(chckbxNewCheckBox);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Clasificaciones ", null, panel_2, null);
		panel_2.setLayout(null);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(10, 11, 63, 43);
		panel_2.add(spinner);
		
		JSlider slider = new JSlider();
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMajorTickSpacing(10);
		slider.setBounds(10, 102, 200, 51);
		panel_2.add(slider);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setForeground(Color.RED);
		progressBar.setValue(50);
		progressBar.setBounds(21, 182, 189, 25);
		panel_2.add(progressBar);
	}
}
